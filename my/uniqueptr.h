#pragma once

template <class T>
class UniquePtr {
    private:
        T* ptr_;

    public:

    template<typename... Args>
    UniquePtr(Args &&... args)
        : ptr_(new T(static_cast<Args &&>(args)...))
    {}

    UniquePtr(T* ptr = nullptr) : ptr_(ptr) {}

    ~UniquePtr() {
        delete ptr_;
    }

    UniquePtr(UniquePtr && other) noexcept : ptr_(other.ptr_) {
       other.ptr_ = nullptr;
    }
    
    UniquePtr& operator=(UniquePtr && other) noexcept {
        if (this != &other) {
            this->~UniquePtr();
            new (this) UniquePtr(static_cast<UniquePtr &&>(other));
        }
        return *this;
    }

    T* operator->() const {
        return ptr_;
    }

    T& operator*() const {
        return *ptr_;
    }

    void swap(UniquePtr& other) {
        auto a = ptr_;
        ptr_ = other.ptr_;
        other.ptr_ = a;
        //std::swap(ptr_, other.ptr_);
    }

    explicit operator bool() const {
        return (ptr_ != nullptr);
    }

    void reset(T* ptr = nullptr) {
        delete ptr_;
        ptr_ = ptr;
    }

    T* release() {
        T* ptr = ptr_;
        ptr_ = nullptr;
        return ptr;
    }
};