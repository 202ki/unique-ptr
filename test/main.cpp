#include <my/uniqueptr.h>
#include <gtest/gtest.h>

#include <utility>


TEST(UP, pointer)
{
    UniquePtr<int> chlen(1);
    EXPECT_EQ(*chlen, 1);
}

TEST(UP, assignment)
{
    UniquePtr<int> chlen(1);
    UniquePtr<int> jopa(3);
    jopa = std::move(chlen);
    EXPECT_EQ(*jopa, 1);
}

TEST(UP, swap)
{
    UniquePtr<int> chlen(1);
    UniquePtr<int> jopa(3);
    UniquePtr<int> piece(0);
    piece = std::move(chlen);
    chlen = std::move(jopa);
    jopa = std::move(piece);
    EXPECT_EQ(*chlen, 3);
    EXPECT_EQ(*jopa, 1);
}

TEST(UP, bol)
{
    UniquePtr<int> chlen(1);
    UniquePtr<int> jopa;
    EXPECT_EQ(static_cast <bool>(chlen), true);
    EXPECT_EQ(static_cast <bool>(jopa), false);
}

TEST(UP, arrow)
{
    UniquePtr<int> chlen(1);
    EXPECT_EQ(*(chlen.operator->()), 1);
}

TEST(UP, move)
{
    UniquePtr<int> chlen(1);
    UniquePtr<int> jopa(std::move(chlen));
    EXPECT_EQ(*jopa, 1);
    EXPECT_EQ(static_cast <bool>(chlen), false);
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
